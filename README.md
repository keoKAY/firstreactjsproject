## Note
this is a very simple project of reactjs

### Key Notes
* we use `react-bootstrap bootstrap ` for the css styling 
* we use `react-router-dom` v6, for navigation to different pages setup

#### Our project structure 
```
.
├── autopush.sh
├── package.json
├── package-lock.json
├── public
│   ├── favicon.ico
│   ├── index.html
│   ├── logo192.png
│   ├── logo512.png
│   ├── manifest.json
│   └── robots.txt
├── README.md
└── src
    ├── App.js
    ├── components
    │   ├── AdminGreeting.js
    │   ├── AppNavBar.js
    │   ├── Greeting.js
    │   ├── Task.js
    │   ├── UserCard.jsx
    │   ├── UserGreeting.js
    │   └── UserProfile.js
    ├── index.css
    ├── index.js
    ├── pages
    │   ├── HomePage.js
    │   └── ProductPage.js
    └── styles
        └── UserProfile.css

```