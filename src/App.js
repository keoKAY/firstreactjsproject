// import UserCard from "./components/UserCard";
import 'bootstrap/dist/css/bootstrap.min.css'
import UserProfile from './components/UserProfile';
import Task from './components/Task';
import AdminGreeting from './components/AdminGreeting';
import UserGreeting from './components/UserGreeting';
import Greeting from './components/Greeting';
import AppNavBar from './components/AppNavBar';
import { BrowserRouter, Route, Router, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage';
import ProductPage from './pages/ProductPage';
import NotFoundndPage from './pages/NotFoundndPage';
import ServicePage from './pages/ServicePage';
import UsersPage from './pages/UsersPage';
function AppfirstVersion() {

  let bonaInfo = {
    username: "Bona Chan",
    gender: "male",
    profileImgUrl: "https://img.freepik.com/premium-psd/3d-male-cute-cartoon-character-avatar-isolated-3d-rendering_235528-1290.jpg",
    bio: "Study hard, and enjoy the process of learningg!!"
  }

  // 5 tasks
  let allTask = [
    {
      taskName: "First tasks",
      description: "This is task description!"
    },

    {
      taskName: "Second tasks",
      description: "This is task description!"
    },

    {
      taskName: "Third tasks",
      description: "This is task description!"
    },

    {
      taskName: "Final tasks",
      description: "This is task description!"
    },
    {
      taskName: "latest  tasks",
      description: "This is task description!"
    },

  ]
  allTask = []

  let isAdmin = false
  return (
    <>

      {/* testing on the conditional rendering */}

      <div className='text-center mt-3'>
        <Greeting isAdmin={isAdmin} />
        {/*       
        {
          isAdmin ? <AdminGreeting /> : <UserGreeting />
        } */}
      </div>
      <div className='container mt-4 d-flex gap-5' >
        <div>
          <h1> User Infor !</h1>
          <UserProfile userData={bonaInfo} buttonMessage="Explore More" />
        </div>



        <div className=' w-75'>
          <h1> All Tasks </h1>

          {
            // allTask.length == 0 ? <div className='no-item-found text-center'>
            //   <img className='img-fluid w-25' src="https://cdn3d.iconscout.com/3d/premium/thumb/search-not-found-5342748-4468820.png?f=webp" alt="" />
            //   <h4 className='text-center'>There is no tasks....!</h4>
            // </div>
            // :
            // <></>

            allTask.length == 0 && <div className='no-item-found text-center'>
              <img className='img-fluid w-25' src="https://cdn3d.iconscout.com/3d/premium/thumb/search-not-found-5342748-4468820.png?f=webp" alt="" />
              <h4 className='text-center'>There is no tasks....!</h4>
            </div>

          }

          {
            allTask.map((task, index) => <Task key={index} taskData={task} />)
          }
        </div>









        {/* <UserCard userData=... />
        <UserCard username="bona" gender="male" />
        <UserCard username="freya" gender="female" />
        <UserCard username="superman" gender="male" /> */}
      </div>
    </>

  );
}
function App() {
  return (
    <>
      <BrowserRouter>
        <AppNavBar />
        <Routes>
          <Route index element={<HomePage />} />
          <Route path='/product' element={<ProductPage />} />
          <Route path='/whatever'  element={<ServicePage/>}/>
          <Route path='/users'  element={<UsersPage/>}/>
          <Route path='*' element={<NotFoundndPage />} />

        </Routes>
      </BrowserRouter>
{/* npm install react-router-dom */}
    </>
  )
}
export default App;
