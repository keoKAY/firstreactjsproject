import React from 'react'

const AdminGreeting = () => {
    return (
        <div>
            <h3> Good to have you back! Mr.Admin</h3>
            <p> As an admin , you basically can do anything!</p>
        </div>
    )
}

export default AdminGreeting