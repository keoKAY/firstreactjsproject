import React, { useState } from 'react'

const CounterFuncComponent = () => {
    const [count, setCount] = useState(0)
    const onIncrease = () => {
        setCount(count + 1)
    }
    const onDecrease = () => {
        setCount(count - 1)
    }
    return (
        <div className='text-center'>
            <h4>Counter from functional component</h4>
            <h1> {count}</h1>
            <div>
                <button
                    className='btn btn-success mx-1'
                    onClick={onIncrease}>Increase</button>
                <button
                    className='btn btn-warning'
                    disabled={count == 0 ? true : false}
                    onClick={onDecrease} >Decrease</button>
            </div>
        </div>
    )
}
export default CounterFuncComponent