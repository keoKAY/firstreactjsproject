// rafce, rfce
//  this is the UserCard use with normal css 
import React from "react";
import "../styles/UserProfile.css";
const UserCard = (props) => {
  let headerStyle = {
    padding:'5px ',
    color: "white",
    backgroundColor: "black",
  };
  return (
    <div
      className="userCard"
      style={{
        backgroundColor: "#FEECE2",
        padding: "5px 10px",
        margin: "20px auto",
        borderRadius:'20px'
      }}
    >
      <h1 style={headerStyle}>{props.username}'s profile card</h1>
      <p>Gender:{props.gender}</p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Sint, velit
        dicta doloribus, ab tenetur cupiditate cumque saepe consectetur itaque
        accusantium praesentium earum dolorem. Asperiores reprehenderit
        doloribus nulla totam blanditiis. Odit!
      </p>
      <button className="btn btn-warning">Explore</button>
    </div>
  );
};

export default UserCard;
