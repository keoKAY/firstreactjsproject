// rafce , rfce
import React from 'react'
import { Button, Card } from 'react-bootstrap';

// destructure the props
function UserProfile({ userData ,buttonMessage}) {
    const {username,profileImgUrl,bio,gender} = userData; 
    return (
        <Card className='task-card' style={{ width: '18rem' }}>
            <Card.Img variant="top" src={profileImgUrl} />
            <Card.Body>
                <Card.Title>{username}</Card.Title>
                <Card.Text>
                    <span>Gender: {gender}</span> <br />
                    {bio}
                </Card.Text>
                <Button classNamevariant="warning">{buttonMessage}</Button>
            </Card.Body>
        </Card>
    );
}

export default UserProfile