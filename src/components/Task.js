import React from 'react'

const Task = ({ taskData }) => {
  const { taskName, description } = taskData;

  return (
    <div className='w-100 rounded bg-light mt-3 py-3 px-4 task-card'>
      <h4 className='text-success'> {taskName} </h4>
      <p> {description}</p>
    </div>
  )
}

export default Task