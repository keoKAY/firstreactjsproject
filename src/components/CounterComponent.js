import React, { Component } from 'react'

// functional component -> stateless component means no state
// class component -> stateful component means state
//  state is a object that is privately maintained inside a component
export class CounterComponent extends Component {

  constructor(props) {
    super(props)
    // this.counter = 0; // this is where we use normal variables 
    this.state = {
      counter: 0
    }
  }

  render() {
    const onIncrease = () => {
      this.setState(
        {
          counter: this.state.counter + 1
        }
      )
      console.log(this.state.counter)
      // alert("Increase is clicked!! ")

    }

    const onDecrease = () => {
      alert("Decrease is clicked!! ")

    }
    return (
      <div className='text-center'>
        <h4>Counter from Class component</h4>
        <h1> {this.state.counter} </h1>
        <div>
          <button className='btn btn-success mx-1' onClick={onIncrease}>Increase</button>
          <button className='btn btn-warning' onClick={onDecrease}>Decrease</button>
        </div>
      </div>
    )
  }
}

export default CounterComponent