import { Button } from 'react-bootstrap';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { NavLink } from 'react-router-dom';

function AppNavBar() {
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="mx-auto ">
            <NavLink className={"nav-link"} to={"/"}>Home</NavLink>
            <NavLink className={"nav-link"} to={"/product"}>Product</NavLink>
            <NavLink className="nav-link" to="/whatever">Service</NavLink>
            <NavLink className="nav-link" to="/users">Users</NavLink>

            <Nav.Link href="#home">Contact Us</Nav.Link>

          </Nav>
          <Button className='mx-2' variant='warning'> Sign Up</Button>
          <Button variant='success'> Login</Button>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default AppNavBar;