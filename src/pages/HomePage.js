import React from 'react'
import CounterComponent from '../components/CounterComponent'

const HomePage = () => {
    return (
        <div className='container'>
            <h1>This is the home page </h1>
            <p> Lorem ipsum dolor sit amet consectetur adipisicing elit. Reprehenderit nisi vitae animi, delectus odit nemo voluptatibus? Quia tenetur fugit magni maxime, quidem eveniet laudantium, consequuntur molestiae labore mollitia enim nesciunt!</p>


            <CounterComponent />
        </div>
    )
}

export default HomePage