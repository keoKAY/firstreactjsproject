import React from 'react'
import CounterFuncComponent from '../components/CounterFuncComponent'

const ProductPage = () => {
    return (
        <div className='container'>
            <h1> This is the product page ! </h1>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore harum est at quo consequuntur, quod beatae! Inventore aperiam autem similique, impedit asperiores laborum sit soluta iste saepe cumque dolore temporibus?</p>
        
            <CounterFuncComponent/>

        </div>
    )
}

export default ProductPage