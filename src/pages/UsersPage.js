import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { getAllUsers } from '../services/UserService'

const UsersPage = () => {
    const [users, setUsers] = useState([])

   
    useEffect(() => {
        getAllUsers()
            .then(res => {
                // we successfully get the data back from the request 
                setUsers(res)
                console.log(res)
            })
            .catch(err => console.log("Error fetching users", err))
    }, [])




    return (
        <div className='container'>

            <h1>All Users names: </h1>
            {
                users.map((user, index) => {
                    return <h3 key={index}>{user.name}</h3>
                })
            }

        </div>
    )
}

export default UsersPage