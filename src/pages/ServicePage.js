import React, { useEffect, useRef, useState } from 'react'
// useState
// useRef
// useEffect

const ServicePage = () => {
    const [username, setUsername] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [role, setRole] = useState('')
    const [selectedFile, setSelectedFile] = useState(null)

    //  what we want is an object of user
    const userData = {
        username, email, password,
        role: role == 1 ? "Customer" : "Admin"
    }

    console.log("User Data", userData)
    const handleImageChange = (e) => {
        let imageUrl = URL.createObjectURL(e.target.files[0])
        // console.log(imageUrl)
        setSelectedFile(imageUrl)
    }
    return (
        <div className='container mx-auto '>

            <div className="container mt-5 d-flex gap-3 justify-content-center bg-light rounded py-5 ">

                <div className='container w-25 flex-column text-center w-25'>
                    <img
                        className='img-fluid rounded '
                        width={"270px"}
                        src={selectedFile ? selectedFile : "https://static.vecteezy.com/system/resources/previews/003/337/511/non_2x/default-avatar-photo-placeholder-profile-icon-female-vector.jpg"}
                        alt="Image Profile for user " />
                    <input
                        className='form-control mt-2'
                        onChange={handleImageChange}
                        type="file"
                        name=""
                        id="" />
                </div>



                <div className=' w-75'>
                    <div className="d-flex gap-3">
                        <div className="mb-3">
                            <label htmlFor="userNameInput" className="form-label">Username</label>
                            <input
                                type="email"
                                value={username}
                                onChange={(e) => setUsername(e.target.value)}
                                className="form-control" id="userNameInput"
                                placeholder="John Doe" />
                        </div>
                        <div className="mb-3 w-100">
                            <label htmlFor="emailInput" className="form-label">Email address</label>

                            <input
                                type="email"
                                className="form-control" id="emailInput"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="name@example.com" />
                        </div>
                    </div>

                    <div className="mb-3">
                        <label htmlFor="exampleFormControlInput1" className="form-label"> Passwsord</label>
                        <input
                            type="password"
                            className="form-control"
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            id="exampleFormControlInput1"
                            placeholder="*******" />
                    </div>
                    <select
                        className="form-select"
                        aria-label="Default select example"
                        value={role}
                        onChange={(e) => setRole(e.target.value)}
                    >
                        <option selected>Choose your role </option>
                        <option value="1">Customer</option>
                        <option value="2">Admin</option>

                    </select>

                    <button className='btn btn-success mt-2'>Sumbit</button>
                </div>

            </div>
        </div>
    )
}

export default ServicePage